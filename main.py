# Python
from typing import Optional
from enum import Enum

# Pydantic
from pydantic import BaseModel
from pydantic import Field
from pydantic import EmailStr, PositiveInt, HttpUrl, SecretStr, SecretBytes

# FastAPI
from fastapi import FastAPI
from fastapi import status
from fastapi import HTTPException
from fastapi import Path, Body, Query, Form, Header, Cookie, File, UploadFile

app = FastAPI()

# Models
class HairColor(Enum):
    white = "white"
    brown = "brown"
    black = "black"
    blonde = "blonde"
    red = "red"

class PersonBase(BaseModel):
    first_name: str = Field(
        ...,
        min_length=1,
        max_length=50,
        example="Joseph"
    )
    last_name: str = Field(
        ...,
        min_length=1,
        max_length=50,
        example="Kahn"
    )
    age: PositiveInt = Field(
        ...,
        example=27
        )
    hair_color: Optional[HairColor] = Field(
        default=None,
        example="black"
    )
    is_married: Optional[bool] = Field(
        default=None,
        example=False
    )
    email: EmailStr = Field(
        ...,
        example="joseph.kahncasapia@gmail.com"
    )
    site_web: Optional[HttpUrl] = Field(
        default=None,
        example="https://jkahn.gitlab.io"
    )

    # class Config:
    #     schema_extra = {
    #         "example" : {
    #             "first_name" : "Joseph",
    #             "last_name" : "kahn",
    #             "age" : 27,
    #         }
    #     }

class Person(PersonBase):
    password: SecretStr = Field(
        ...,
        min_length=8,
        example="holasoyunejemplo"
    )

class PersonOut(PersonBase):
    pass

class LoginBase(BaseModel):
    username: str = Field(
        ...,
        max_length=20,
        example="kanino19"
    )
    message: str = Field(default="Login Succesfully")

class Login(LoginBase):
    password: SecretStr = Field(
        ...,
        min_length=8,
        example="Holasoyunejemplo"
    )

class LoginOut(LoginBase):
    pass

class Location(BaseModel):
    city: str = Field(
        ...,
        min_length=1,
        max_length=50,
        example="Lima"
    )
    state: str = Field(
        ...,
        min_length=1,
        max_length=50,
        example="Lima"
    )
    country: str = Field(
        ...,
        min_length=1,
        max_length=50,
        example="Peru"
    )

# Home
@app.get(
    path="/",
    status_code=status.HTTP_200_OK,
    tags=["home"],
    summary="Hello World"
)
def home():
    """
    Hello World

    This path operations show a hellow world message on the screen

    Returns a json with the key 'Hello' and value 'World'
    """
    return {"Hello": "World"}

# Request and response body
@app.post(
    path="/person/new",
    response_model=PersonOut,
    status_code=status.HTTP_201_CREATED,
    tags=["Persons"],
    summary="Create Person in the app"
)
def create_person(person: Person = Body(...)):
    """
    Create Person

    This path operation creates a person in the app and save the information in the database

    Parameters:
    - Request body parameter:
        - **person: Person** -> A person model with first name, last name, age, hair color, marital status, email, site_web and password

    Returns a person model with first name, last name, age, hair color and marital status
    """
    return person

# Validations: Query parameters
@app.get(
    path="/person/detail",
    status_code=status.HTTP_200_OK,
    tags=["Persons"],
    summary="Show a person details",
    deprecated=True
)
def show_person(
    name: Optional[str] = Query(
        default=None,
        min_length=1,
        max_length=50,
        title="Person Name",
        description="This is the person name, it's between 1 and 50 characters",
        example="Joseph"
        ),
    age: int = Query(
        ...,
        gt=0,
        title="Person Age",
        description="This is the person age. It's required"
        )
):
    """
    Show person

    This path operations writo on the screen the person details

    Parameters
    - Request query parameter:
        - **name: str** -> A person name string
        - **age: int** -> A person age integer

    Returns a dicctionary with the key 'name' and value 'age'
    """
    return {name: age}

persons = [1,2,3,4,5]

# Validactions: Path parameters
@app.get(
    path="/person/detail/{person_id}",
    status_code=status.HTTP_200_OK,
    tags=["Persons"],
    summary="Show a person details using a person id"
)
def show_person(
    person_id: int = Path(
        ...,
        gt=0,
        title="Person Identification",
        description="This is the person identification. It's required"
        )
):
    """
    Show person details with a person id

    This path operations show a person details using the pat parameter person_id

    Parameters:
    - Path parameters:
        - **person_id: int** -> A person id integer

    Returns a json with key 'person_id' and value 'It exists!'
    """
    if person_id not in persons:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="¡This person doesn't exist!"
        )
    return {person_id: "It exists!"}

# Validations: Request body
@app.put(
    path="/person/{person_id}",
    status_code=status.HTTP_200_OK,
    tags=["Persons"],
    summary="Update person information"
)
def update_person(
    person_id: int = Path(
        ...,
        title="Person ID",
        description="This is the person ID",
        gt=0
    ),
    person: Person = Body(...),
    location: Location = Body(...)
):
    """
    Uptade person information

    This path operations update the person details

    Parameters:
    - Path parameters:
        - **person_id: int** -> A person id

    - Request body parameters
        - **person: Person** -> A person model with first name, last name, age, hair color, marital status, email, site_web and password
        - **location: Location** -> A location model with city, state and country

    Returns a json with the person and location objects
    """
    results = person.dict()
    results.update(location.dict())
    return results

@app.post(
    path="/login",
    response_model=LoginOut,
    status_code=status.HTTP_200_OK,
    tags=["Persons"],
    summary="Login a user"
)
def login(
    username: str = Form(...),
    password: str = Form(...)
):
    """
    Login a user

    This path operations login with a username and password

    Parameters:
    - Request form parameters:
        - **username: str** -> a username
        - **password: str** -> a password

    Returns a Login object of the username and password
    """
    return Login(username=username, password=password)

# Cookies and Headers
@app.post(
    path="/contact",
    status_code=status.HTTP_200_OK,
    tags=["info"],
    summary="Get user agent and cookies"
)
def contact(
    first_name: str = Form(
        ...,
        max_length=20,
        min_lenght=1,
    ),
    last_name: str = Form(
        ...,
        max_length=20,
        min_lenght=1,
    ),
    email: EmailStr = Form(...),
    message: str = Form(
        ...,
        min_length=20
    ),
    user_agent: Optional[str] = Header(default=None),
    ads: Optional[str] = Cookie(default=None)
):
    """
    Get hearders and cookies

    This path operations get the headers and cookies client

    Parameters:
    - Request form parameters:
        - **first_name: str** -> a first name
        - **last_name: str** -> a last name
        - **email: EmailStr** -> a EmailStr model from pydantic
        - **message: str** -> a message from the user
    - Request Header parameters:
        - **user_agent: str** -> a header model
    - Request Cookie parameters:
        - **ads: str** -> a cookies model

    Returns a user agent from the header
    """
    return user_agent

# files
@app.post(
    path="/post-image",
    status_code=status.HTTP_200_OK,
    tags=["info"],
    summary="Upload a images"
)
def post_image(
    image: UploadFile = File(...)
):
    """
    Upload a image

    This path operations uploads a image

    Parameters:
    - Request file parameters:
        - **image: UploadFile** -> a UploadFile model upload a image

    Returns the image details
    """
    return {
        "Filename": image.filename,
        "Format": image.content_type,
        "Size(kb": round(len(image.file.read())/1024, ndigits=2)
    }